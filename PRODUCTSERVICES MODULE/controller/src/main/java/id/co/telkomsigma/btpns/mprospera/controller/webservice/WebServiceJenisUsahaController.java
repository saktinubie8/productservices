package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.CheckVersionRequest;
import id.co.telkomsigma.btpns.mprospera.request.ListJURequest;
import id.co.telkomsigma.btpns.mprospera.response.JUResponse;
import id.co.telkomsigma.btpns.mprospera.response.JenisUsahaVersionResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListJUResponse;
import id.co.telkomsigma.btpns.mprospera.service.JenisUsahaService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webserviceJenisUsahaController")
public class WebServiceJenisUsahaController extends GenericController {

	@Autowired
	private TerminalService terminalService;

	@Autowired
	private TerminalActivityService terminalActivityService;

	@Autowired
	private WSValidationService wsValidationService;

	@Autowired
	private JenisUsahaService jenisUsahaService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	JsonUtils jsonUtils = new JsonUtils();

	@RequestMapping(value = WebGuiConstant.TERMINAL_GET_CHECK_VERSION_JENIS_USAHA, method = { RequestMethod.POST })
	public @ResponseBody JenisUsahaVersionResponse getCheckVersion(@RequestBody final CheckVersionRequest request,
			@PathVariable("apkVersion") String apkVersion) {
		log.info("Incoming Message For validating Version");

		String username = request.getUsername();
		String imei = request.getImei();
		String sessionKey = request.getSessionKey();
		String retrievalReferenceNumber = request.getRetrievalReferenceNumber();

		final JenisUsahaVersionResponse versionResponse = new JenisUsahaVersionResponse();

		versionResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
			log.info("Check Version INCOMING MESSAGE : " + jsonUtils.toJson(request));
			String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				versionResponse.setResponseCode(validation);
				log.error("Validation Failed for Username : " + username + "imei : " + imei + "Session Key : "
						+ sessionKey);
				String label = getMessage("webservice.rc.label." + versionResponse.getResponseCode());
				versionResponse.setResponseMessage(label);
			} else {
				Date lastDateData = jenisUsahaService.getLastData();
				java.sql.Date dateSQL = new java.sql.Date(lastDateData.getTime());
				versionResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
				versionResponse.setResponseMessage("SUCCESS");
				versionResponse.setLastUpdateDate(dateSQL);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			try {
				log.info("Try to create Terminal Activity");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei(imei);
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_JENIS_USAHA);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(retrievalReferenceNumber);
						terminalActivity.setSessionKey(sessionKey);
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername(username);
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
					}
				});
				log.info("check Version Jenis Usaha RESPONSE MESSAGE : " + jsonUtils.toJson(versionResponse));
			} catch (Exception e) {
				log.error("check Version Jenis Usaha saveTerminalActivityAndMessageLogs error: " + e.getMessage());
			}
		}
		return versionResponse;
	}

	@RequestMapping(value = WebGuiConstant.PATH_DOWNLOAD_JENIS_USAHA_REQUEST, method = { RequestMethod.POST })
	public void downloadFiles(final HttpServletRequest request, final HttpServletResponse response,
			@PathVariable("apkVersion") String apkVersion,
			@RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
			@RequestHeader(value = "retrievalReferenceNumber", required = true) String retrievalReferenceNumber,
			@RequestHeader(value = "username", required = true) String username,
			@RequestHeader(value = "sessionKey", required = true) String sessionKey,
			@RequestHeader(value = "imei", required = true) String imei) throws IOException {
		final ListJUResponse responseCode = new ListJUResponse();

		CheckVersionRequest incomingMessage = new CheckVersionRequest();
		incomingMessage.setTransmissionDateAndTime(transmissionDateAndTime);
		incomingMessage.setRetrievalReferenceNumber(retrievalReferenceNumber);
		incomingMessage.setImei(imei);
		incomingMessage.setUsername(username);
		incomingMessage.setSessionKey(sessionKey);

		responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
		InputStream inputStream = null;
		ZipEntry eNtry = null;
		ZipOutputStream zippedOut = null;
		FileSystemResource resource = null;
		try {
			log.info("INCOMING MESSAGE Check Version BusinessType : " + jsonUtils.toJson(incomingMessage));
			String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				responseCode.setResponseCode(validation);
				log.error("Validation Failed for Username : " + username + "imei : " + imei + "Session Key : "
						+ sessionKey);
				String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
				responseCode.setResponseMessage(label);
			} else {
				log.info("Validation Success, get Jenis Usaha Data");
				List<JenisUsaha> listJenisUsaha = jenisUsahaService.getAllJenisUsaha();
				JenisUsaha lastIndex = null;
				if (!listJenisUsaha.isEmpty()) {
					lastIndex = listJenisUsaha.get(listJenisUsaha.size() - 1);
					log.info("last index : " + lastIndex);
				}
				for (JenisUsaha jenisUsahaFromList : listJenisUsaha) {
					JUResponse jUsaha = new JUResponse();
					jUsaha.setId(jenisUsahaFromList.getId());
					jUsaha.setTujuanPembiayaan(jenisUsahaFromList.getTujuanPembiayaan());
					jUsaha.setbusinessCategory(jenisUsahaFromList.getKatBidangUsaha());
					jUsaha.setbusinessType(jenisUsahaFromList.getDescUsaha());
					jUsaha.setsubBusiness(jenisUsahaFromList.getSandiBaruDesc());
					responseCode.getBusinessTypeList().add(jUsaha);
				}
				log.info("GET data Jenis Usaha Success");
				responseCode.setLastDataDate(
						lastIndex.getCreatedDate().toString() + "_" + lastIndex.getCreatedDate().getTime());
				ObjectMapper mapper = new ObjectMapper();

				File file = new File("businessType.json");
				try {
					// Serialize Java object info JSON file.
					mapper.writeValue(file, responseCode);

				} catch (IOException e) {
					e.printStackTrace();
				}

				response.setContentType("application/octet-stream");

				response.setHeader("Content-Disposition", "attachment; filename=businessType.zip");

				response.setContentLength((int) file.length());
				zippedOut = new ZipOutputStream(response.getOutputStream());
				resource = new FileSystemResource(file);
				eNtry = new ZipEntry(resource.getFilename());
				eNtry.setSize(resource.contentLength());
				zippedOut.putNextEntry(eNtry);
			}
			String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
			responseCode.setResponseCode(label);
		} catch (Exception e) {
			log.error("list Jenis Usaha ERRROR : " + e.getMessage());
			responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
			responseCode.setResponseMessage("list Jenis Usaha ERROR : " + e.getMessage());
		} finally {
			try {
				log.info("Try to create Terminal Activity");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei(imei);
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_JENIS_USAHA);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(retrievalReferenceNumber);
						terminalActivity.setSessionKey(sessionKey);
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername(username);
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
					}
				});
				// log.info("check Version Jenis Usaha RESPONSE MESSAGE : " +
				// jsonUtils.toJson(response));
			} catch (Exception e) {
				log.error("check Version Jenis Usaha saveTerminalActivityAndMessageLogs error: " + e.getMessage());
			}
		}
		StreamUtils.copy(resource.getInputStream(), zippedOut);
		zippedOut.closeEntry();
		zippedOut.finish();
		zippedOut.flush();
	}

	@RequestMapping(value = WebGuiConstant.TERMINAL_GET_JENIS_USAHA_LINK_REQUEST, method = { RequestMethod.POST })
	public void listJu(@RequestBody final ListJURequest request, HttpServletResponse response,
			@PathVariable("apkVersion") String apkVersion, @PathVariable("fileName") String fileName) throws Exception {

		String username = request.getUsername();
		String imei = request.getImei();
		String sessionKey = request.getSessionKey();
		final ListJUResponse responseCode = new ListJUResponse();
		responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
			log.info("List Jenis Usaha INCOMING MESSAGE : " + jsonUtils.toJson(request));
			String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				responseCode.setResponseCode(validation);
				log.error("Validation Failed for Username : " + username + "imei : " + imei + "Session Key : "
						+ sessionKey);
				String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
				responseCode.setResponseMessage(label);
			} else {
				log.info("Validation Success, get Jenis Usaha Data");
				List<JenisUsaha> listJenisUsaha = jenisUsahaService.getAllJenisUsaha();
				JenisUsaha lastIndex = null;
				if (!listJenisUsaha.isEmpty()) {
					lastIndex = listJenisUsaha.get(listJenisUsaha.size() - 1);
					log.info("last index : " + lastIndex);
				}
				for (JenisUsaha jenisUsahaFromList : listJenisUsaha) {
					JUResponse jUsaha = new JUResponse();
					jUsaha.setId(jenisUsahaFromList.getId());
					jUsaha.setTujuanPembiayaan(jenisUsahaFromList.getTujuanPembiayaan());
					jUsaha.setbusinessCategory(jenisUsahaFromList.getKatBidangUsaha());
					jUsaha.setbusinessType(jenisUsahaFromList.getDescUsaha());
					jUsaha.setsubBusiness(jenisUsahaFromList.getSandiBaruDesc());
					responseCode.getBusinessTypeList().add(jUsaha);
				}
				log.info("GET data Jenis Usaha Success");
				responseCode.setLastDataDate(
						lastIndex.getCreatedDate().toString() + "_" + lastIndex.getCreatedDate().getTime());
				ObjectMapper mapper = new ObjectMapper();

				File file = new File("Mapping_Jenis_Usaha.json");
				try {
					// Serialize Java object info JSON file.
					mapper.writeValue(file, responseCode);

				} catch (IOException e) {
					e.printStackTrace();
				}

				response.setContentType("application/octet-stream");

				response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

				response.setContentLength((int) file.length());

				InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

				FileCopyUtils.copy(inputStream, response.getOutputStream());
			}
			String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
			responseCode.setResponseCode(label);
		} catch (Exception e) {
			log.error("list Jenis Usaha ERRROR : " + e.getMessage());
			responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
			responseCode.setResponseMessage("list Jenis Usaha ERROR : " + e.getMessage());
		} finally {
			try {
				log.info("Try to create Terminal Activity");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_JENIS_USAHA);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
						terminalActivity.setSessionKey(request.getSessionKey());
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername(request.getUsername().trim());
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
					}
				});
				log.info("listSda RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
			} catch (Exception e) {
				log.error("listSda saveTerminalActivityAndMessageLogs error: " + e.getMessage());
			}
		}
	}
}
