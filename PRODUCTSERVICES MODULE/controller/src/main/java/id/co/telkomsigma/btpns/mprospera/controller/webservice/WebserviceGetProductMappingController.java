package id.co.telkomsigma.btpns.mprospera.controller.webservice;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.ProductPlafond;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.CheckVersionRequest;
import id.co.telkomsigma.btpns.mprospera.request.MappingProductRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.response.MappingProductVersionResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.service.ProductMappingService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;



@Controller("webserviceGetProductMappingController")
public class WebserviceGetProductMappingController extends GenericController {
    @Autowired
    private SWService swService;
	
    @Autowired
    private ProductMappingService productMappingService;
    
	@Autowired
	private TerminalService terminalService;
	
	@Autowired
	private WSValidationService wsValidationService;
	
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Autowired
	private TerminalActivityService terminalActivityService;
    
	JsonUtils jsonUtils = new JsonUtils();
    
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_MAPPING_PRODUCT_VERSION,method = {RequestMethod.POST})
    public @ResponseBody String cekVersiFileMappingProduct(HttpServletRequest request,HttpServletResponse response,@PathVariable ("version") Long version) throws ParseException{
		log.info("Incoming Message For validating Version");
		Date date = new Date(version);
		log.info("VERSION_date : "+date);
		java.sql.Date dateSQL = new java.sql.Date(date.getTime());
		log.info("VERSION_dateSQL : "+dateSQL);
    	String mappingProductCekVersi = productMappingService.getVersionByUpdateDate(date,version);
    	log.info(mappingProductCekVersi.toString());
    	return mappingProductCekVersi;
    }
    
    @RequestMapping(value = WebGuiConstant.TERMINAL_VERSION_MAPPING_PRODUCT_REQUEST, method = { RequestMethod.POST })
	public @ResponseBody MappingProductVersionResponse getCheckVersion(@RequestBody final CheckVersionRequest request,
			@PathVariable("apkVersion") String apkVersion) {
    	
		String username = request.getUsername();
		String imei = request.getImei();
		String sessionKey = request.getSessionKey();
		String retrievalReferenceNuber = request.getRetrievalReferenceNumber();
    	
		final MappingProductVersionResponse mappingProductVersionResponse = new MappingProductVersionResponse();
		
		mappingProductVersionResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
			log.info("Check Version INCOMING MESSAGE : " + jsonUtils.toJson(request));
			String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				mappingProductVersionResponse.setResponseCode(validation);
				log.error("Validation Failed for Username : " + username + "imei : " + imei + "Session Key : "
						+ sessionKey);
				String label = getMessage("webservice.rc.label." + mappingProductVersionResponse.getResponseCode());
				mappingProductVersionResponse.setResponseMessage(label);
			}else {
				Date lastData = productMappingService.findByUpdateDate();
				java.sql.Date dateSQL = new java.sql.Date(lastData.getTime());
				mappingProductVersionResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
				mappingProductVersionResponse.setResponseMessage("SUCCESS");
				mappingProductVersionResponse.setLastUpdateDate(dateSQL);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			try {
				log.info("Try to create Terminal Activity");
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei(imei);
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_VERSION_MAPPING_PRODUCT);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(retrievalReferenceNuber);
						terminalActivity.setSessionKey(sessionKey);
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername(username);
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
					}
				});
			} catch (Exception e) {
				log.error("check Version Jenis Usaha saveTerminalActivityAndMessageLogs error: " + e.getMessage());
			}
		}
    	return mappingProductVersionResponse;
    }
    
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_MAPPING_PRODUCT_REQUEST, method = {RequestMethod.POST})
    public void getMappingProductAll(final HttpServletResponse response,final HttpServletRequest request, 
    		@PathVariable("apkVersion") String apkVersion,
			@RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
			@RequestHeader(value = "retrievalReferenceNumber", required = true) String retrievalReferenceNumber,
			@RequestHeader(value = "username", required = true) String username,
			@RequestHeader(value = "sessionKey", required = true) String sessionKey,
			@RequestHeader(value = "imei", required = true) String imei) throws IOException{
		final ListProductMappingResponse listProductMappingResponse = new ListProductMappingResponse();
		
		final MappingProductRequest requestMapping = new MappingProductRequest();
		requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        
        listProductMappingResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);

        try {
			log.info("getDataMappingProduct INCOMING MESSAGE");  
	        log.info("Validating Request");
			log.info("List Mapping Product INCOMING MESSAGE : " + jsonUtils.toJson(requestMapping));
			String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				listProductMappingResponse.setResponseCode(validation);
				log.error("Validation Failed for Username : " + username + "imei : " + imei + "Session Key : "
						+ sessionKey);
				String label = getMessage("webservice.rc.label." + listProductMappingResponse.getResponseCode());
				listProductMappingResponse.setResponseMessage(label);
			}else {
				log.info("Validation Success, get Loan Mapping Product");
				LoanProduct lastUpdateDate = null;
				List<LoanProduct> listproductMapping =  productMappingService.getAllLoanProductId();

				List<ProductMappingResponse> productMappingResponse = new ArrayList<>();
				String kosong = " ";
				for(LoanProduct loanProdutMappingFromList : listproductMapping) {
					ProductMappingResponse productMapping = new ProductMappingResponse();
					productMapping.setProductId(loanProdutMappingFromList.getProductId());					
					if(loanProdutMappingFromList.getProductName() != null) {
					productMapping.setProductName(loanProdutMappingFromList.getProductName());
					}else {
						productMapping.setProductName(kosong);
					}					
					if(loanProdutMappingFromList.getInstallmentCount() != null) {
					productMapping.setJumlahAngsur(loanProdutMappingFromList.getInstallmentCount());
					}else {
						productMapping.setJumlahAngsur(0);
					}				
					if(loanProdutMappingFromList.getTenor() != null) {
					productMapping.setTenorMinggu(loanProdutMappingFromList.getTenor());
					}else {
						productMapping.setTenorMinggu(0);
					}
					if(loanProdutMappingFromList.getTenorBulan() != null) {
						productMapping.setTenorBulan(loanProdutMappingFromList.getTenorBulan());
					}else {
						productMapping.setTenorBulan(0);						
					}
					if(loanProdutMappingFromList.getLoanType() != null) {
					productMapping.setTipePembiayaan(loanProdutMappingFromList.getLoanType());
					}else {
						productMapping.setTipePembiayaan(kosong);
					}
					if(loanProdutMappingFromList.getStatus()!=null) {
						productMapping.setStatus(loanProdutMappingFromList.getStatus());
					}else {
						productMapping.setStatus(kosong);
					}			
					if(loanProdutMappingFromList.getInstallmentFreqTime() != null) {
					productMapping.setFrekuensiAngsurSatuan(loanProdutMappingFromList.getInstallmentFreqTime());
					}else {
						productMapping.setFrekuensiAngsurSatuan(kosong);
					}
					if(loanProdutMappingFromList.getInstallmentFreqCount()!= null) {
					productMapping.setFrekuensiAngsur(loanProdutMappingFromList.getInstallmentFreqCount());
					}else {
						productMapping.setFrekuensiAngsur(0);
					}
					if(loanProdutMappingFromList.getMargin() != null) {
						productMapping.setMarjin(loanProdutMappingFromList.getMargin().abs(MathContext.DECIMAL32));	
					}
					else {
						productMapping.setMarjin(loanProdutMappingFromList.getMargin().ZERO);	
					}
		            if(loanProdutMappingFromList.getProductRate()!=null) {
		            	productMapping.setProductRate(loanProdutMappingFromList.getProductRate());
		            }else {
		            	productMapping.setProductRate(loanProdutMappingFromList.getProductRate().ZERO);
		            }
		            List<ProductPlafond> plafonList = swService.findByProductId(loanProdutMappingFromList);
		            List<BigDecimal> plafonNominalList = new ArrayList<>();
		            List<BigDecimal> kosongBigArray = new ArrayList<>();
		            for (ProductPlafond plafon : plafonList) {
		                BigDecimal nominal;
		                nominal = plafon.getPlafond();
		                if (plafon.getPlafond() == null) {
		                    nominal = BigDecimal.ZERO;
		                }
		                plafonNominalList.add(nominal);
		            }   
		            if(plafonNominalList.isEmpty()) {
		            productMapping.setPlafon(kosongBigArray);
		            }else {
		                productMapping.setPlafon(plafonNominalList);
		            }		            
		            if(loanProdutMappingFromList.getIir()!= null) {
					productMapping.setIir(loanProdutMappingFromList.getIir());
		            }else {
		            	productMapping.setIir(loanProdutMappingFromList.getIir().ZERO);
		            }	            
		            if(loanProdutMappingFromList.getJenisNasabah()!=null) {
					productMapping.setTrxType(loanProdutMappingFromList.getJenisNasabah());
		            }else {
		    			productMapping.setTrxType(kosong);
		            }		            
		            if(loanProdutMappingFromList.getTujuanpembiayaan()!=null) {
					productMapping.setTujuanpembiayaan(loanProdutMappingFromList.getTujuanpembiayaan());
		            }else {
		    			productMapping.setTujuanpembiayaan(kosong);
		            }		            
		            if(loanProdutMappingFromList.getRegularPiloting()!=null) {
					productMapping.setWismaStatus(loanProdutMappingFromList.getRegularPiloting());
		            }else {
		    			productMapping.setWismaStatus(kosong);
		            }
		            
		            if(loanProdutMappingFromList.getTypeNasabah()!= null) {
		            	productMapping.setCustomerType(loanProdutMappingFromList.getTypeNasabah());
		            }else {
		            	productMapping.setCustomerType(kosong);
		            }
		                 
					List<LoanProduct> listdDetailProductMapping = productMappingService.getLoanProductId(loanProdutMappingFromList.getProductId());
					if(!listdDetailProductMapping.isEmpty()) {
						lastUpdateDate = listdDetailProductMapping.get(listdDetailProductMapping.size()-1);
						log.info("lastUpdateDate : "+lastUpdateDate);
					}
					productMappingResponse.add(productMapping);
				}
				
				listProductMappingResponse.setProductList(productMappingResponse);
				log.info("GET data mapping product Success");
				log.info("HASIL JSON " +listProductMappingResponse.toString());
				ObjectMapper mapper = new ObjectMapper();	
				File file = new File("Products.json");
				try {
					// Serialize Java object info JSON file.
					mapper.writeValue(file, listProductMappingResponse);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				String filejson = "Products.zip";

				response.setContentType("application/octet-stream");

				response.setHeader("Content-Disposition", "attachment; filename="+filejson);

				response.setContentLength((int) file.length());
				
				try(ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())){
					FileSystemResource resource = new FileSystemResource(file);
					ZipEntry e = new ZipEntry(resource.getFilename());
					e.setSize(resource.contentLength());
					zippedOut.putNextEntry(e);
					StreamUtils.copy(resource.getInputStream(), zippedOut);
					zippedOut.closeEntry();
					zippedOut.finish();
					zippedOut.flush();
				}
			}
			String label = getMessage("webservice.rc.label." + listProductMappingResponse.getResponseCode());
			listProductMappingResponse.setResponseCode(label);
		} catch (Exception e) {
			log.error("Mapping Product ERRROR : " + e.getMessage());
			listProductMappingResponse.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
			listProductMappingResponse.setResponseMessage("Mapping Product ERROR : " + e.getMessage());
		}finally {
			try {
				log.info("Try to create Terminal Activity");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei(imei);
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_LIST_MAPPING_PRODUCT);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(retrievalReferenceNumber.trim());
						terminalActivity.setSessionKey(sessionKey);
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername(username.trim());
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
					}
				});
				log.info("list RESPONSE MESSAGE : " + jsonUtils.toJson(listProductMappingResponse));
			} catch (Exception e) {
				log.error("list saveTerminalActivityAndMessageLogs error: " + e.getMessage());
			}
		}
		
	}
    
    
	//Mapping Product per Id sebagai percobaan
	@RequestMapping(value = WebGuiConstant.TERMINAL_BUSINESS_MAPPING_PRODUCT_LINK_REQUEST, method = {RequestMethod.GET})
    public void getMappingProductId(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String filename,@PathVariable("id") Long id)throws IOException {
		super.setResponseAsZip(response);
		final ListProductMappingResponse listProductMappingResponse = new ListProductMappingResponse();
		listProductMappingResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
		
		log.info("Validation Success, get Loan Mapping Product");
		LoanProduct lastUpdateDate = null;
		List<LoanProduct> listproductMapping =  productMappingService.getFindLoanByProductId(id);

		List<ProductMappingResponse> productMappingResponse = new ArrayList<>();
		String kosong = " ";
		for(LoanProduct loanProdutMappingFromList : listproductMapping) {
			ProductMappingResponse productMapping = new ProductMappingResponse();
			productMapping.setProductId(loanProdutMappingFromList.getProductId());
			
			if(loanProdutMappingFromList.getProductName() != null) {
			productMapping.setProductName(loanProdutMappingFromList.getProductName());
			}else {
				productMapping.setProductName(kosong);
			}
			
			if(loanProdutMappingFromList.getInstallmentCount() != null) {
			productMapping.setJumlahAngsur(loanProdutMappingFromList.getInstallmentCount());
			}else {
				productMapping.setJumlahAngsur(0);

			}
			
			if(loanProdutMappingFromList.getTenor() != null) {
			productMapping.setTenorMinggu(loanProdutMappingFromList.getTenor());
			}else {
				productMapping.setTenorMinggu(0);
			}
			
			if(loanProdutMappingFromList.getTenorBulan() != null) {
				productMapping.setTenorBulan(loanProdutMappingFromList.getTenorBulan());
			}else {
				productMapping.setTenorBulan(0);						
			}
			
			if(loanProdutMappingFromList.getLoanType() != null) {
			productMapping.setTipePembiayaan(loanProdutMappingFromList.getLoanType());
			}else {
				productMapping.setTipePembiayaan(kosong);
			}
			
			if(loanProdutMappingFromList.getStatus()!=null) {
				productMapping.setStatus(loanProdutMappingFromList.getStatus());
			}else {
				productMapping.setStatus(kosong);

			}			
			if(loanProdutMappingFromList.getInstallmentFreqTime() != null) {
			productMapping.setFrekuensiAngsurSatuan(loanProdutMappingFromList.getInstallmentFreqTime());
			}else {
				productMapping.setFrekuensiAngsurSatuan(kosong);
			}
			
			if(loanProdutMappingFromList.getInstallmentFreqCount()!= null) {
			productMapping.setFrekuensiAngsur(loanProdutMappingFromList.getInstallmentFreqCount());
			}else {
				productMapping.setFrekuensiAngsur(0);
			}
			if(loanProdutMappingFromList.getMargin() != null) {
				productMapping.setMarjin(loanProdutMappingFromList.getMargin().abs(MathContext.DECIMAL32));	
			}
			else {
				productMapping.setMarjin(loanProdutMappingFromList.getMargin().ZERO);	
			}
            if(loanProdutMappingFromList.getProductRate()!=null) {
            	productMapping.setProductRate(loanProdutMappingFromList.getProductRate());
            }else {
            	productMapping.setProductRate(loanProdutMappingFromList.getProductRate().ZERO);
            }
            List<ProductPlafond> plafonList = swService.findByProductId(loanProdutMappingFromList);
            List<BigDecimal> plafonNominalList = new ArrayList<>();
            List<BigDecimal> kosongBigArray = new ArrayList<>();
            for (ProductPlafond plafon : plafonList) {
                BigDecimal nominal;
                nominal = plafon.getPlafond();
                if (plafon.getPlafond() == null) {
                    nominal = BigDecimal.ZERO;
                }
                plafonNominalList.add(nominal);
            }
            
            if(plafonNominalList.isEmpty()) {
            productMapping.setPlafon(kosongBigArray);
            }else {
                productMapping.setPlafon(plafonNominalList);
            }
            
            if(loanProdutMappingFromList.getIir()!= null) {
			productMapping.setIir(loanProdutMappingFromList.getIir());
            }else {
            	productMapping.setIir(loanProdutMappingFromList.getIir().ZERO);
            }
            
            if(loanProdutMappingFromList.getJenisNasabah()!=null) {
			productMapping.setTrxType(loanProdutMappingFromList.getJenisNasabah());
            }else {
    			productMapping.setTrxType(kosong);
            }
            
            if(loanProdutMappingFromList.getTujuanpembiayaan()!=null) {
			productMapping.setTujuanpembiayaan(loanProdutMappingFromList.getTujuanpembiayaan());
            }else {
    			productMapping.setTujuanpembiayaan(kosong);
            }
            if(loanProdutMappingFromList.getTypeNasabah()!= null) {
            	productMapping.setCustomerType(loanProdutMappingFromList.getTypeNasabah());
            }else {
            	productMapping.setCustomerType(kosong);
            }
               
            
            if(loanProdutMappingFromList.getRegularPiloting()!=null) {
			productMapping.setWismaStatus(loanProdutMappingFromList.getRegularPiloting());
            }else {
    			productMapping.setWismaStatus(loanProdutMappingFromList.getRegularPiloting());
            }
            
			List<LoanProduct> listdDetailProductMapping = productMappingService.getLoanProductId(loanProdutMappingFromList.getProductId());
			if(!listdDetailProductMapping.isEmpty()) {
				lastUpdateDate = listdDetailProductMapping.get(listdDetailProductMapping.size()-1);
				log.info("lastUpdateDate : "+lastUpdateDate);
			}
			productMappingResponse.add(productMapping);
		}
		listProductMappingResponse.setProductList(productMappingResponse);

		log.info("GET data mapping product Success");
		log.info("HASIL JSON " +listProductMappingResponse.toString());
		ObjectMapper mapper = new ObjectMapper();	
		File file = new File("mappingProduct.json");
		try {
			// Serialize Java object info JSON file.
			mapper.writeValue(file, listProductMappingResponse);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String filejson = "mappingProduct.zip";

		response.setContentType("application/octet-stream");

		response.setHeader("Content-Disposition", "attachment; filename="+filejson);

		response.setContentLength((int) file.length());
		
		try(ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())){
			FileSystemResource resource = new FileSystemResource(file);
			ZipEntry e = new ZipEntry(resource.getFilename());
			e.setSize(resource.contentLength());
			zippedOut.putNextEntry(e);
			StreamUtils.copy(resource.getInputStream(), zippedOut);
			zippedOut.closeEntry();
			zippedOut.finish();
			zippedOut.flush();
		}

	}

}
