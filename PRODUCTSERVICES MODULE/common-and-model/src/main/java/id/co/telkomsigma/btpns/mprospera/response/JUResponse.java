package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("all")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class JUResponse {

	private Long id;
	private String tujuanPembiayaan;
	private String businessCategory;
	private String businessType;
	private Long kdSectorUsaha;
	private String subBusiness;
	private Date createdDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTujuanPembiayaan() {
		return tujuanPembiayaan;
	}
	public void setTujuanPembiayaan(String tujuanPembiayaan) {
		this.tujuanPembiayaan = tujuanPembiayaan;
	}
	public String getbusinessCategory() {
		return businessCategory;
	}
	public void setbusinessCategory(String businessCategory) {
		this.businessCategory = businessCategory;
	}
	public String getbusinessType() {
		return businessType;
	}
	public void setbusinessType(String businessType) {
		this.businessType = businessType;
	}
	public Long getKdSectorUsaha() {
		return kdSectorUsaha;
	}
	public void setKdSectorUsaha(Long kdSectorUsaha) {
		this.kdSectorUsaha = kdSectorUsaha;
	}
	public String getsubBusiness() {
		return subBusiness;
	}
	public void setsubBusiness(String subBusiness) {
		this.subBusiness = subBusiness;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
