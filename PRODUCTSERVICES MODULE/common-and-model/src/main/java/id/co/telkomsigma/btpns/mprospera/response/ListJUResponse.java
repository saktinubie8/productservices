package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ListJUResponse extends BaseResponse {

	private String grandTotal;
	private String currentTotal;
	private String totalPage;
	private String lastDataDate;
	private List<JUResponse> businessTypeList;
	
	
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getCurrentTotal() {
		return currentTotal;
	}
	public void setCurrentTotal(String currentTotal) {
		this.currentTotal = currentTotal;
	}
	public String getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}

	public List<JUResponse> getBusinessTypeList() {
		if(businessTypeList == null)
			businessTypeList = new ArrayList<>();
		return businessTypeList;
	}
	public void setBusinessTypeList(List<JUResponse> businessTypeList) {
		this.businessTypeList = businessTypeList;
	}
	public String getLastDataDate() {
		return lastDataDate;
	}
	public void setLastDataDate(String lastDataDate) {
		this.lastDataDate = lastDataDate;
	}
}
