package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@SuppressWarnings("all")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ProductMappingResponse{
    private Long productId;
    private String productName;
    private Integer jumlahAngsur;
    private Integer tenorMinggu;
    private Integer tenorBulan;
    private String tipePembiayaan;
    private String frekuensiAngsurSatuan;
    private Integer frekuensiAngsur;
    private String status;
    private BigDecimal marjin;
    private BigDecimal productRate;
	private List<BigDecimal> plafon;
    private BigDecimal iir;
	private String trxType;
	private String tujuanpembiayaan; 
	private String wismaStatus;
	private String customerType;
	


	public Long getProductId() {
		return productId;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Integer getJumlahAngsur() {
		return jumlahAngsur;
	}


	public void setJumlahAngsur(Integer jumlahAngsur) {
		this.jumlahAngsur = jumlahAngsur;
	}


	public Integer getTenorMinggu() {
		return tenorMinggu;
	}


	public void setTenorMinggu(Integer tenorMinggu) {
		this.tenorMinggu = tenorMinggu;
	}

	public Integer getTenorBulan() {
		return tenorBulan;
	}


	public void setTenorBulan(Integer tenorBulan) {
		this.tenorBulan = tenorBulan;
	}
	
	public String getTipePembiayaan() {
		return tipePembiayaan;
	}


	public void setTipePembiayaan(String tipePembiayaan) {
		this.tipePembiayaan = tipePembiayaan;
	}


	public String getFrekuensiAngsurSatuan() {
		return frekuensiAngsurSatuan;
	}


	public void setFrekuensiAngsurSatuan(String frekuensiAngsurSatuan) {
		this.frekuensiAngsurSatuan = frekuensiAngsurSatuan;
	}


	public Integer getFrekuensiAngsur() {
		return frekuensiAngsur;
	}


	public void setFrekuensiAngsur(Integer frekuensiAngsur) {
		this.frekuensiAngsur = frekuensiAngsur;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public BigDecimal getMarjin() {
		return marjin;
	}


	public void setMarjin(BigDecimal marjin) {
		this.marjin = marjin;
	}


	public BigDecimal getProductRate() {
		return productRate;
	}


	public void setProductRate(BigDecimal productRate) {
		this.productRate = productRate;
	}


	public List<BigDecimal> getPlafon() {
		return plafon;
	}


	public void setPlafon(List<BigDecimal> plafon) {
		this.plafon = plafon;
	}


	public BigDecimal getIir() {
		return iir;
	}


	public void setIir(BigDecimal iir) {
		this.iir = iir;
	}


	public String getTrxType() {
		return trxType;
	}


	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}


	public String getTujuanpembiayaan() {
		return tujuanpembiayaan;
	}


	public void setTujuanpembiayaan(String tujuanpembiayaan) {
		this.tujuanpembiayaan = tujuanpembiayaan;
	}


	public String getWismaStatus() {
		return wismaStatus;
	}


	public void setWismaStatus(String wismaStatus) {
		this.wismaStatus = wismaStatus;
	}


	public String getCustomerType() {
		return customerType;
	}


	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}


	@Override
	public String toString() {
		return "ProductMappingResponse [productId=" + productId + ", productName=" + productName + ", jumlahAngsur="
				+ jumlahAngsur + ", tenorMinggu=" + tenorMinggu + ", tenorBulan=" + tenorBulan + ", tipePembiayaan="
				+ tipePembiayaan + ", frekuensiAngsurSatuan=" + frekuensiAngsurSatuan + ", frekuensiAngsur="
				+ frekuensiAngsur + ", status=" + status + ", marjin=" + marjin + ", productRate=" + productRate
				+ ", plafon=" + plafon + ", iir=" + iir + ", trxType=" + trxType + ", tujuanpembiayaan="
				+ tujuanpembiayaan + ", wismaStatus=" + wismaStatus + ", customerType=" + customerType + "]";
	}







	



	




	



}
