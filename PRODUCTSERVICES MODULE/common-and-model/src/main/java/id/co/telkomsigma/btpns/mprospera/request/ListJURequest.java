package id.co.telkomsigma.btpns.mprospera.request;

public class ListJURequest extends BaseRequest {

	private String transmissionDateAndTime;
	private String retrievalReferenceNumber;
	private String username;
	private String sessionKey;
	private String imei;
	private String areaCategory;
	private Integer getCountData;
	private Integer page;
	private String startLookupDate;
	private String endLookupDate;
	
	
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getAreaCategory() {
		return areaCategory;
	}
	public void setAreaCategory(String areaCategory) {
		this.areaCategory = areaCategory;
	}
	public Integer getGetCountData() {
		return getCountData;
	}
	public void setGetCountData(Integer getCountData) {
		this.getCountData = getCountData;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getStartLookupDate() {
		return startLookupDate;
	}
	public void setStartLookupDate(String startLookupDate) {
		this.startLookupDate = startLookupDate;
	}
	public String getEndLookupDate() {
		return endLookupDate;
	}
	public void setEndLookupDate(String endLookupDate) {
		this.endLookupDate = endLookupDate;
	}
}
