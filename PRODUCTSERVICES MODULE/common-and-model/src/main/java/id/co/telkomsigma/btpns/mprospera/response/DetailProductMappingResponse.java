package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("all")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class DetailProductMappingResponse {
	private Long mappingProductId;
	private Long productIdMapping;
	private String status;
	private String jenisMMS;
	private String ketentuan;
	private String tujuanPembiayaan;
	private String regularPiloting;
	private int plafondMin;
	private int plafonMax;
	private Date updateDate;
	
	
	
	
	public Long getMappingProductId() {
		return mappingProductId;
	}
	public void setMappingProductId(Long mappingProductId) {
		this.mappingProductId = mappingProductId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJenisMMS() {
		return jenisMMS;
	}
	public void setJenisMMS(String jenisMMS) {
		this.jenisMMS = jenisMMS;
	}
	public String getKetentuan() {
		return ketentuan;
	}
	public void setKetentuan(String ketentuan) {
		this.ketentuan = ketentuan;
	}
	public String getTujuanPembiayaan() {
		return tujuanPembiayaan;
	}
	public void setTujuanPembiayaan(String tujuanPembiayaan) {
		this.tujuanPembiayaan = tujuanPembiayaan;
	}
	public String getRegularPiloting() {
		return regularPiloting;
	}
	public void setRegularPiloting(String regularPiloting) {
		this.regularPiloting = regularPiloting;
	}
	public int getPlafondMin() {
		return plafondMin;
	}
	public void setPlafondMin(int plafondMin) {
		this.plafondMin = plafondMin;
	}
	public int getPlafonMax() {
		return plafonMax;
	}
	public void setPlafonMax(int plafonMax) {
		this.plafonMax = plafonMax;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getProductIdMapping() {
		return productIdMapping;
	}
	public void setProductIdMapping(Long productIdMapping) {
		this.productIdMapping = productIdMapping;
	}
	
	
	
	
}
