package id.co.telkomsigma.btpns.mprospera.response;

import java.sql.Date;

public class JenisUsahaVersionResponse extends BaseResponse {
	
	public String checkValue;
	
	public Date lastUpdateDate;
	
	

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getCheckValue() {
		return checkValue;
	}

	public void setCheckValue(String checkValue) {
		this.checkValue = checkValue;
	}
}
