package id.co.telkomsigma.btpns.mprospera.response;
import java.sql.Date;


public class MappingProductVersionResponse  extends BaseResponse{
	public Date lastUpdateDate;
	
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
