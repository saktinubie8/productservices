package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    ParamManager paramManager;
    @Autowired
    TerminalManager terminalManager;
    @Autowired
    UserManager userManager;



    public void clearParamCache() {
        paramManager.clearCache();
    }




    public void clearTerminalCache() {
        terminalManager.clearCache();
    }

    public void clearUserCache() {
        userManager.clearCache();
    }

    public void clearCache() {
        paramManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
    }

}