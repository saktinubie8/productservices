package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.LoanProductDao;

import id.co.telkomsigma.btpns.mprospera.dao.ProductPlafondDao;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.ProductPlafond;

@SuppressWarnings("RedundantIfStatement")
@Service("swManager")
public class SWManagerImpl implements SWManager{


    @Autowired
    private LoanProductDao loanProductDao;


    @Autowired
    private ProductPlafondDao productPlafondDao;


    @PersistenceContext
    public EntityManager em;




    @Override
    @CacheEvict(value = {"wln.sw.countAllSW", "wln.sw.countAllSWByStatus", "wln.neighbor.findNeighborBySwId",
            "wln.sw.findSwMapApprovalBySwId", "wln.sw.findByLocalId", "wln.loan.product.findAllProduct"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }
 

    @Override
    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        // TODO Auto-generated method stub
        return productPlafondDao.findByProductIdOrderByPlafond(productId);
    }


    @Override
    public Page<LoanProduct> findAllProductPageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(pageRequest);
    }



}
