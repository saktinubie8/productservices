package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

public interface LoanProductMappingManager {
	
    int countAll();
    

    Page<LoanProduct> findAll(List<String> kelIdList);
    
//    Page<LoanProduct> findAll();

    Page<LoanProduct> findAllProductMapping();

    Page<LoanProduct> findAllProductMappingPageable(PageRequest pageRequest);
//
//    Page<LoanProduct> findAllByCreatedDate(Date startDate, Date endDate);
//
//    Page<LoanProduct> findAllByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest);
//
//    Page<LoanProduct> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);
//
//    Page<LoanProduct> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
//                                                PageRequest pageRequest);
												
    void save(LoanProduct LoanProduct);

//    Boolean isValidProductMapping(String productId);

    //LoanProduct findByRrn(String rrn);

//    void clearCache();
//    
    //Json
    List<LoanProduct> getLoanProductId(Long productIdMapping);
    
    List<LoanProduct> getAllLoanProductId();
    
    List<LoanProduct> getFindLoanByProductId(Long productId);
    
    
    LoanProduct getVersionByUpdateDate (Date updateDate);
    
    LoanProduct findTopOneByUpdateDateOrderByUpdateDateDesc (Date updateDate);
    
    LoanProduct findTop1ByUpdateDateOrderByUpdateDateDesc();
    
    
//    List<DetailMappingProduct> findByProductIdMapping(LoanProduct productId);
    
  //  List<DetailMappingProduct> findByProductIdMapping(LoanProduct productId);

    
    
}
