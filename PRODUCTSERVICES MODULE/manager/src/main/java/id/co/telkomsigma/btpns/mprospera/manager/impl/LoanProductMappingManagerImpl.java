package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.LoanProductMappingDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanProductMappingManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

@SuppressWarnings("RedundantIfStatement")
@Service("loanProductMappingManager")
public class LoanProductMappingManagerImpl implements LoanProductMappingManager {
	
	@Autowired
	private LoanProductMappingDao loanProductMappingDao;
	
//	@Autowired
//	private DetailMappingProductDao detailMappingProductDao;
	@Override
	public int countAll() {
		return loanProductMappingDao.countAll();
	}

//	@Override
//	public Page<LoanProduct> findAll() {
//		return null;
//	}
//	
	@Override
	public Page<LoanProduct> findAll(List<String> kelIdList) {
		return loanProductMappingDao.findByProductId(kelIdList, new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<LoanProduct> findAllProductMapping() {
		return loanProductMappingDao.findAll(new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<LoanProduct> findAllProductMappingPageable(PageRequest pageRequest) {
		return loanProductMappingDao.findAll(pageRequest);
	}

//	@Override
//	public Page<LoanProduct> findAllByCreatedDate(Date startDate, Date endDate) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Page<LoanProduct> findAllByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Page<LoanProduct> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Page<LoanProduct> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
//			PageRequest pageRequest) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void save(LoanProduct LoanProduct) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public Boolean isValidProductMapping(String productId) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	@Override
//	public void clearCache() {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
    //@Cacheable(value = "wln.sw.findByLocalId", unless = "#result == null")
	public List<LoanProduct> getLoanProductId(Long productIdMapping) {
		return loanProductMappingDao.findByproductId(productIdMapping);
	}

	@Override
	public List<LoanProduct> getAllLoanProductId() {
		return loanProductMappingDao.findAll();
	}

	//find by Id
	@Override
	public List<LoanProduct> getFindLoanByProductId(Long productId) {
		return loanProductMappingDao.findByproductId(productId);
	}

	@Override
	public LoanProduct getVersionByUpdateDate(Date updateDate) {
		return loanProductMappingDao.getTopOneByupdateDate(updateDate);
	}

	@Override
	public LoanProduct findTopOneByUpdateDateOrderByUpdateDateDesc(Date updateDate) {
		
		return loanProductMappingDao.findTopOneByUpdateDateGreaterThanOrderByUpdateDateDesc(updateDate);
	}

	@Override
	public LoanProduct findTop1ByUpdateDateOrderByUpdateDateDesc() {
		return loanProductMappingDao.findTop1ByOrderByUpdateDateDesc();
	}

//	@Override
//	public List<DetailMappingProduct> findByProductIdMapping(LoanProduct productId) {
//		return detailMappingProductDao.findByproductIdMapping(productId);
//	}

	

}
