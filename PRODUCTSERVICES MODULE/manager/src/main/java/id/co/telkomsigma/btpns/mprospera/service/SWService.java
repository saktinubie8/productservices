package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("swService")
public class SWService extends GenericService {

    @Autowired
    @Qualifier("swManager")
    private SWManager swManager;

    @Autowired
    private UserManager userManager;

    
    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        return swManager.findByProductId(productId);
    }

//    public LoanProduct findByProductId(String productId) {
//        return swManager.findByProductId(productId);
//    }

    

    

}