package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {

    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private UserDao userDao;

    @Override
   // @Cacheable(value = "product.user.userByUsername", unless = "#result == null")
    public User getUserByUsername(String username) {
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    public User getUserByUserId(Long userId) {
        User user = userDao.findByUserId(userId);
        return user;
    }

    @Override
   // @Cacheable(value = "product.user.sessionKeyByUsername", unless = "#result == null")
    public String getSessionKeyByUsername(String username) {
        log.info("direct to DB");
        return userDao.findSessionKeyByUsername(username.toLowerCase());
    }

    @Override
   // @CacheEvict(value = {"product.user.userByUsername", "product.user.userByLocId", "product.user.sessionKeyByUsername"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
   // @Cacheable(value = "product.user.userByLocId", unless = "#result == null")
    public List<User> getUserByLocId(String locId) {
        // TODO Auto-generated method stub
        List<User> userWisma = userDao.findByOfficeCode(locId);
        return userWisma;
    }

    @Override
    public List<User> findUserNonMS() {
        String role = "3";
        return userDao.findByRoleUser(role);
    }

}