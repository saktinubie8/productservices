package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.*;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface SWManager {


    Page<LoanProduct> findAllProductPageable(PageRequest pageRequest);


    List<ProductPlafond> findByProductId(LoanProduct productId);


    void clearCache();

}